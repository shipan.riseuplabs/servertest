var express = require("express");
var app = express();
app.get("/", (req, res) => {
    return res.send("<h2>Welcome to Express App<h2>");
  });
app.listen(3000, (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log("Server is listening at : 3000");
    }
  });